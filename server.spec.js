const { app, server } = require('./server.js');
const request = require('supertest')
it('tests /test endpoint for a 200 reponse', async () => {
    await request(app)
        .get('/test')
        .expect(200)
})

afterAll(async () => {
    await server.close();
})