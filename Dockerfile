# Build image we will copy necessary files from later to minimize image size
FROM node:21.6.1-bullseye-slim as builder
# Installing dumb init system to ensure all signals (SIGINT, etc) are passed properly to the Node app
RUN apt-get update && apt-get install -y --no-install-recommends dumb-init

WORKDIR /app
# Copying the local files into the Docker image with the --chown flag because containers shouldn't be run as root for security reasons.
COPY --chown=node:node package*.json /app/
# npm ci allows for failure when there is any difference at all between package-lock.json files
RUN npm ci --only=production
# enables performance and security optimizations for Express and is generally good practice for Node applications
ENV NODE_ENV production



# Production image 
FROM node:21.6.1-bullseye-slim as production
COPY --from=builder /usr/bin/dumb-init /usr/bin/dumb-init
USER node
WORKDIR /app
COPY --chown=node:node --from=builder /app/node_modules /app/node_modules
# copying over anything that isn't in the .dockerignore 
COPY --chown=node:node . /app
ENV NODE_ENV production
EXPOSE 30007

CMD ["dumb-init", "node", "server.js"]