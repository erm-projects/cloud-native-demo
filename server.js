const express = require('express');
const app = express();
const PORT = '30007';
// Test comment for demo
app.get('/test', (req, res) => {
    res.send({
        message: "Liatrio",
        timestamp: Date.now()
    });
})

const server = app.listen(PORT, () => {
    console.log(`Server listening on port: ${PORT}`)
})

module.exports = { app, server }