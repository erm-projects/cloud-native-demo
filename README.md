### API
There is a sample Node.js REST API found in server.js to use for demonstration and testing.

#### Run API locally
    1. Clone the project
    2. cd into project directory 
    3. `npm install`
    4. `node server.js`
    5. `npm run test` to run automated tests for the app

#### Run API in a Docker Container
    1. Clone the project
    2. cd into project directory (where Dockerfile is located)
    3. `docker build -t cloud-native-demo .`
    4. `docker run -p 3000:3000 --name=cloud-native-demo cloud-native-demo`

#### Push images to ECR
The Gitlab pipeline automatically builds, tests, and pushes images up to the ECR repo defined in the .gitlab-ci.yml variables section.  To create a new image, make your code change locally and then push it to Gitlab.  The pipeline will automatically run and push the image if all jobs succeed.  